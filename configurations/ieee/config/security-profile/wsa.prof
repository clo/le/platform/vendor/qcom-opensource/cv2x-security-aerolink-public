#  Copyright (c) 2020, The Linux Foundation. All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#    * Neither the name of The Linux Foundation nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
#  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
#  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
#  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
#  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
#  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

### Configuration for WSA (Wave Service Advertisement from RSU) security profile
### per IEEE 1609.3.

psid  0x87

# Interval, in ms, at which full certificates are included in signed SPDUs.
certificate_interval                     495    # WSA profile requires .495 sec.

##############################################################################
### Set which fields are included in the security headers.

include_generation_time                 true    # WSA profile requires true

include_expiry_time                     true    # WSA profile requires true

    # SPDU expiry time is set to generation time plus this value, in ms.
    spdu_lifetime                      30000    # WSA profile requires 30 sec.

include_generation_location             true    # WSA profile requires true

##############################################################################
### Set which relevance checks are executed.

check_replays                          false    # WSA profile requires false

check_relevance_past_freshness         false    # WSA profile requires false

    # Maximum time in the past, in us, to accept an SPDU as valid.
    past_freshness_tolerance               0    # arbitrary, not used

check_relevance_future_freshness        true    # WSA profile requires true

    # Maximum time in the future, in us, to accept an SPDU as valid.
    future_freshness_tolerance      60000000    # WSA profile requires 60 sec.

check_relevance_expiry_time             true    # WSA profile requires true

check_relevance_generation_location    false    # WSA profile requires false

    # Maximum distance from receiver, in meters, to accept an SPDU as valid.
    distance_tolerance                     0    # arbitrary, not used

check_relevance_certificate_expiry      true    # WSA profile requires true

##############################################################################
### Set which consistency checks are executed.

check_consistency_generation_location   true    # WSA profile requires true

##############################################################################
### Whether or not to perform CRL checks for this PSID

check_crl                              false    # WSA profile requires false

##############################################################################
### Set how long SPDUs are valid, in seconds, after the last CRL expires.

overdue_crl_tolerance                      0    # arbitrary, not used

##############################################################################
### Peer To Peer Certificate Distribution or Certificate Learning or
### Unknown Certificate Request

# Set if and how certificates are requested and provided if unknown to the
# receiver.  Choices are none, inline, out_of_band.
p2pcd_flavor                     out_of_band    # WSA profile requires
                                                # out_of_band
